/**
 * @module ApiaryLib
 *
 * @description The API library for Apiary.
 *
 * @copyright Miro Wagner.
 *
 * @license Apache 2.0
 *
 * @example
 * // Debug messages on console.
 * ApiaryLib.DEBUGMODE = true;
 *
 * var apiary = new ApiaryLib.Controller();
 *
 * apiary.init('#apiary');
 *
 * // ...
 * // ...
 *
 * // Optionally, you may later destroy Apiary.
 * apiary.destroy('#apiary');
 *
 */
var ApiaryLib = ApiaryLib || {};
ApiaryLib.DEBUGMODE = false;
