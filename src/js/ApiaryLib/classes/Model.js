/**
 * @constructor
 *
 * @alias Model
 *
 * @description Represents an Apiary instance.
 */
ApiaryLib.Model = function() {
  if (!(this instanceof ApiaryLib.Model)) {
    return new ApiaryLib.Model();
  }

  this.resetData();
};

/**
 * @function
 *
 * @memberof Model
 *
 * @description (Re)set model data.
 *
 * @returns {Boolean} true or false, representing success or failure.
 */
ApiaryLib.Model.prototype.resetData = function() {
  var objDataStructure = {
    objDictionary: {
      objWordsByPoints: {
        3: false,
        1: false
      },
      arrGeneratorWords: false
    },
    objHoneycomb: {
      strLetters: false,
      objWordsByPoints: {
        3: false,
        1: false
      },
      numPointTotal: false
    }
  };

  this.objInternalData = objDataStructure;

  if (this.objInternalData === objDataStructure) {
    return true;
  }
  else {
    return false;
  }
};

/**
 * @function
 *
 * @memberof Model
 *
 * @description Set the dictionary from which Apiary gets words.
 *
 * @arg {Object} ev The event triggering the function.
 * @arg {Object} objDictionary
 */
ApiaryLib.Model.prototype.setDictionary = function(ev, objDictionary) {
  this.objInternalData.objDictionary = objDictionary;
};

/**
 * @function
 *
 * @description Generate a complete puzzle.
 *
 * @emits /view/update
 *
 * @param {Object} ev The event triggering the function.
 * @param {Object} objHoneycombConf
 *        Contains the letters and dictionary from which to generate words.
 */
ApiaryLib.Model.prototype.setHoneycomb = function(ev, objHoneycombConf) {
  objHoneycombConf = objHoneycombConf || {};

  objHoneycombConf.objDictionary =  objHoneycombConf.objDictionary ||
                                    this.objInternalData.objDictionary;

  var objHoneycomb = new ApiaryLib.Honeycomb(objHoneycombConf);

  this.objInternalData.objHoneycomb = objHoneycomb;
  $.publish('/view/update', objHoneycomb);
};
