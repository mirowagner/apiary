/**
 * @alias Controller
 *
 * @description Represents an Apiary instance.
 *
 * @constructor
 *
 * @listens /controller/init
 * @listens /controller/destroy

 * @listens /model/setDictionary
 * @listens /model/setHoneycomb
 * @listens /model/resetData

 * @listens /view/update
 * @listens /view/togglePane
 * @listens /view/selectCell
 *
 */
ApiaryLib.Controller = function() {

  // Enforce stricter constructor behaviour.
  if (!(this instanceof ApiaryLib.Controller)) {
      return new ApiaryLib.Controller();
  }

  // Log events
  if (ApiaryLib.DEBUGMODE === true) {
    var eventstreams = new Array(
      '/controller/init',
      '/controller/destroy',

      '/model/setDictionary',
      '/model/setHoneycomb',
      '/model/resetData',

      '/view/update',
      '/view/togglePane',
      '/view/selectCell'
    );

    eventstreams.forEach(function (cv, index, arr) {
      $.subscribe(cv, function (ev, data) {
        console.log('EVENT:', ev.type);
        console.log('EVENT DATA:', data);
        console.log('---');
      });
    });
	}

  // Data and View objects.
	this.objModel = new ApiaryLib.Model();
  this.objView = new ApiaryLib.View();

  $.subscribe(
    '/controller/init',
    this.init.bind(this)
  );

  $.subscribe(
    '/controller/destroy',
    this.destroy.bind(this)
  );

  $.subscribe(
    '/model/setDictionary',
    this.objModel.setDictionary.bind(this.objModel)
  );

  $.subscribe(
    '/model/setHoneycomb',
    this.objModel.setHoneycomb.bind(this.objModel)
  );

  $.subscribe(
    '/view/update',
    this.objView.updateCells.bind(this.objView)
  );

  $.subscribe(
    '/view/update',
    this.objView.updateResults.bind(this.objView)
  );

  $.subscribe(
    '/model/resetData',
    this.objModel.resetData.bind(this.objModel)
  );

  $.subscribe(
    '/view/togglePane',
    this.objView.togglePane.bind(this.objView)
  );

  $.subscribe(
    '/view/selectCell',
    this.objView.focusInput.bind(this.objView)
  );
};

/**
 * @function
 *
 * @memberof Controller
 *
 * @description Initialize Apiary with a specific element.
 *
 * @arg {Object} ev The event triggering the function.
 * @arg {String} strQuerySelector The element to initialize on.
 *
 * @emits /model/setHoneycomb
 *
 * @throws {Error} strQuerySelector not found
 *
 * @return {Boolean} true if successful.
 */
ApiaryLib.Controller.prototype.init = function(ev, strQuerySelector) {
  if ($(strQuerySelector).length) {

    // Set dictionary
    $.ajax({
      dataType: 'json',
      url: './assets/wordlists/english.json',
      async: false,
      success: function(data) {
        $.publish('/model/setDictionary', data);
      }
    });

    this.objView.create(strQuerySelector);

    $.publish('/model/setHoneycomb');

    return true;

  } else {
    throw new Error('Apiary: ' + strQuerySelector + ' not found.');
  }
};

/**
 * @function
 *
 * @memberof Controller
 *
 * @description Destroy Apiary instance on specified element.
 *
 * @arg {Object} ev The event triggering the function.
 * @arg {String} strQuerySelector The element to initialize on.
 *
 * @throws {Error} strQuerySelector not found
 *
 * @return {Boolean} true if successful.
 */
ApiaryLib.Controller.prototype.destroy = function(strQuerySelector) {

  if ($(strQuerySelector).length) {

    $(strQuerySelector).remove();

    return true;

  } else {
    throw new Error('Apiary: ' + strQuerySelector + ' not found.');
  }

};
