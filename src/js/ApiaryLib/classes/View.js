/**
 * @alias View
 *
 * @description Represents an instance of Apiary's view layer.
 *
 * @constructor
 */
ApiaryLib.View = function() {
  if (!(this instanceof ApiaryLib.View)) {
    return new ApiaryLib.View();
  }
};

/**
 * @function
 *
 * @description Binds events to DOM elements.
 *
 * @memberof View
 *
 * @param {String} strQuerySelector The element on which to bind event handlers.
 *
 * @emits /model/setWords
 * @emits /view/selectCell
 * @emits /view/togglePane
 */
ApiaryLib.View.prototype.create = function(strQuerySelector) {
  // Cache DOM
  this.$apiary = $(strQuerySelector);
  this.$inputs = this.$apiary.find('.js-letter-input');
  this.$cells = this.$apiary.find('.js-cell');
  this.$paneToggles = this.$apiary.find('.js-toggle-pane');

  // Bind inputs
  (function($inputs, $cells) {
    var highlightClass = 'cell--active';
    var strInitialVal;
    var strLetters;
    var objHoneycomb;

    $inputs.each(function() {
      var index = $(this).index();

      $(this).on({
        focus: function() {
          // Store initial value
          strInitialVal = $(this).val();

          // Clear current value
          $(this).val('');

          // Add highlight to target cell
          $cells.eq(index).addClass(highlightClass);
        },
        blur: function() {
          // If value empty, restore initial value
          if (!$(this).val().length) {
            $(this).val(strInitialVal);
          }

          // Remove highlight class from all cells
          $cells.removeClass(highlightClass);
        },
        input: function() {
          // If valid input (alphabet only)
          if ($(this).val().match(/[A-Za-z]/)) {

            // Get current letters
            strLetters = '';
            $inputs.each(function() {
              strLetters += $(this).val();
            });

            // Publish letters
            $.publish('/model/setHoneycomb', {
              strLetters: strLetters.toLowerCase()
            });

            // Focus next input if it exists
            if ($(this).next().length) {
              $.publish('/view/selectCell', index + 1);
            } else {
              $(this).blur();
            }

          // If invalid input, clear value, wait for proper input
          } else {
            $(this).val('');
          }
        }
      });
    });
  }(this.$inputs, this.$cells));

  // Bind cells
  this.$cells.click(function() {
    $.publish('/view/selectCell', $(this).index());
  });

  // Bind toggle pane buttons
  this.$paneToggles.click(function(event) {
    event.preventDefault();
    $.publish('/view/togglePane', {
      strQuerySelector: $(this).attr('href')
    });
  });

  this.$apiary.find('.js-randomize').click(function(event) {
    event.preventDefault();
    $.publish('/model/setHoneycomb');
  });
};

/**
 * @function
 *
 * @memberof View
 *
 * @description focus a letter input element.
 *
 * @param  {Object} ev The event triggering the function.
 * @param  {Number} numIndex The index of the input to focus, from 0-6.
 *
 * @throws {Error} Cannot find input.
 *
 * @return {Boolean} true if successful.
 */
ApiaryLib.View.prototype.focusInput = function(ev, numIndex) {
  var $target = this.$inputs.eq(numIndex);

  if ($target) {
    this.$inputs.eq(numIndex).focus();
    return true;

  } else {
    throw new Error('Apiary: cannot find input with index of ' + numIndex);
  }

};

/**
 * @function
 *
 * @memberof View
 *
 * @description Toggle panes on or off.
 *
 * @param  {Object} ev The event triggering the function.
 * @param  {Object} objPaneConfig Target pane and state to apply.
 */
ApiaryLib.View.prototype.togglePane = function(ev, objPaneConfig) {
  var $targetPane = $(objPaneConfig.strQuerySelector);
  var strState = objPaneConfig.strState;

  switch (strState) {
    case 'show':
      $targetPane.show();
      break;

    case 'hide':
      $targetPane.hide();
      break;

    default:
      if ($targetPane.is(':visible')) {
        $targetPane.hide();
      } else {
        $targetPane.show();
      }
  }
};

/**
 * @function
 *
 * @memberof View
 *
 * @description Updates each svg <text> element, showing letters.
 *
 * @param  {Object} ev The event triggering the function.
 * @param  {Object} objHoneycomb Letters used and computed words.
 */
ApiaryLib.View.prototype.updateCells = function(ev, objHoneycomb) {
  for (var i = 0; i < 7; i++) {
    $('.js-letter-input').eq(i).val(objHoneycomb.strLetters[i]);
    $('.js-cell-text').eq(i).text(objHoneycomb.strLetters[i]);
  }
};

/**
 * @function
 *
 * @memberof View
 *
 * @description Updates results from computed words.
 *
 * @param  {Object} ev The event triggering the function.
 * @param  {Object} objHoneycomb Letters used and computed words.
 *
 * @emits /view/togglePane
 */
ApiaryLib.View.prototype.updateResults = function(ev, objHoneycomb) {

  $.publish('/view/togglePane', {
    strQuerySelector: '#results',
    strState: 'hide'
  });

  var $resultsGroup;
  var $resultsHeader;
  var $resultsList;
  var $resultsWordCount;
  var wordCount;
  var pointCount;
  var word;
  var html;
  var href;

  for (var intPoints in objHoneycomb.objWordsByPoints) {
    $resultsGroup = this.$apiary.find('.js-results-group[data-point-val="' + intPoints + '"]');
    $resultsHeader = $resultsGroup.find('.js-results-header');
    $resultsList = $resultsGroup.find('.js-results-list');
    $resultsWordCount = $resultsGroup.find('.js-word-count');

    wordCount = objHoneycomb.objWordsByPoints[intPoints].length;
    pointCount = wordCount * intPoints;

    $resultsList.empty();
    $resultsWordCount.text('0');

    for (var i = 0; i < wordCount; i++) {
      word = objHoneycomb.objWordsByPoints[intPoints][i];
      href = 'https://www.wordgamedictionary.com/dictionary/word/' + word + '/';

      $resultsWordCount.text(wordCount);

      html = '<li class="results__item">' +
                '<a href="' + href + '" target="_blank">' +
                    word +
                '</a>' +
              '</li>' ;

      $resultsList.append(html);
    }
  }
};
