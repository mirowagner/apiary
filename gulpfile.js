/*
  Contents
  ========

  Require Node modules
  Variables
  Helpers
  Define tasks
*/



/*
  Require Node modules
  ====================

  Manual: Require `gulp` and other Node modules (but not gulp plugins).
*/
var gulp = require('gulp');
var bs = require('browser-sync');
var mainBowerFiles = require('main-bower-files');
var args = require('yargs').argv;

/*
  Automatic: require all gulp plugins found in package.json.

  The plugin becomes a property of the `plugins` object.
  Its name is camelized and drops the `gulp-` prefix,
  e.g. `gulp-some-plugin` becomes `plugins.somePlugin`
*/
var gulpLoadPlugins = require('gulp-load-plugins');
var plugins = gulpLoadPlugins();

/*
  Variables
  =========
*/
var basePaths = {
  src: './src/',
  dest: './build/',
  bower: './bower_components/'
};

if (args.production) {
  basePaths.dest = './public/';
}

var paths = {
  bower: {
    root:     basePaths.bower,
    main:     mainBowerFiles()
  },
  src: {
    root:     basePaths.src,
    assets:   basePaths.src + 'assets/',
    js:       basePaths.src + 'js/',
    sass:     basePaths.src + 'sass/',
    jade:     basePaths.src + 'jade/',
    words:    basePaths.src + 'wordlists/'
  },
  dest: {
    root:     basePaths.dest,
    assets:   basePaths.dest + 'assets/',
    css:      basePaths.dest + 'assets/css/',
    js:       basePaths.dest + 'assets/js/',
    jsdoc:    basePaths.dest + 'doc/',
    words:    basePaths.dest + 'assets/wordlists/'
  }
};


/*
  Helpers
  =======
*/
var handleErrors = function() {
  return plugins.plumber({
    errorHandler: plugins.notify.onError("<%= error.message %>")
  });
};



/*
  Define tasks
  ============

  https://github.com/gulpjs/gulp/blob/master/docs/API.md#gulptaskname--deps--fn
*/


/*
  Bundle Bower dependencies:
*/
gulp.task('vendor-js', function() {
  var filterJS = plugins.filter('**/*.js');
  var filename = 'vendor.min.js';

  return gulp.src(mainBowerFiles({includeDev: args.production ? false : true}))
    .pipe(handleErrors())
    .pipe(filterJS)
    .pipe(plugins.newer(paths.dest.js + filename))
    .pipe(plugins.if(args.production, plugins.uglify()))
    .pipe(plugins.concat(filename))
    .pipe(gulp.dest(paths.dest.js));
});

/*
  Misc. assets
*/
gulp.task('copy-assets', function() {
  return gulp.src(paths.src.assets + '**/*')
    .pipe(gulp.dest(paths.dest.assets));
});


/*
  Compile Jade templates:
*/
gulp.task('jade', function() {
  return gulp.src(paths.src.jade + '*.jade')
    .pipe(handleErrors())
    .pipe(plugins.newer(paths.dest.root + '**/*.html'))
    .pipe(plugins.jade({
      locals: {
        args: args
      },
      pretty: args.production ? false : true
    }))
    .pipe(gulp.dest(paths.dest.root));
});


/*
  Build wordlist:
  Takes a newline-separated list of words.
  Outputs a JSON file:

  {
    "objWordsByPoints": {
      "3": [],
      "1": []
    },
    "arrGeneratorWords": []
  }
*/

gulp.task('build-wordlist', function() {

  // Returns new array without duplicate entries
  function discardDupes(array) {
    return array.sort().filter(function(el, index, self) {
      return index == self.indexOf(el);
    });
  }

  return gulp.src(paths.src.words + '**/*')
    .pipe(plugins.intercept(function(file){

      var arrSourceWords = file.contents.toString().split('\n');
      var arrGeneratorWords = [];
      var objWordsByPoints = {
        3: [],
        1: []
      };

      var strLetters;
      var arrUniqueLetters;
      var numPointVal;

      for (var i = 0; i < arrSourceWords.length; i++) {
        strLetters = arrSourceWords[i];
        arrUniqueLetters = discardDupes(strLetters.split(''));
        strUniqueLetters = arrUniqueLetters.join('');

        // Word is valid if it...
        if (
          // Is at least five letters long,
          strLetters.length > 4 &&
          // only contains lowercase alphas,
          !strLetters.match(/[^a-z]/) &&
          // and has no more than seven unique letters.
          strUniqueLetters.length < 8
        ) {
          // Organize valid words by point value
          numPointVal = strUniqueLetters.length === 7 ? 3 : 1;
          objWordsByPoints[numPointVal].push(strLetters);

          // Find letter combinations ideal for generating words from
          if (
            // Ensures there will be a 3-point word
            numPointVal === 3 &&

            // "s" and "ing" make it less interesting
            !strLetters.includes('s') &&
            (
              !strLetters.includes('i') &&
              !strLetters.includes('n') &&
              !strLetters.includes('g')
            )
          ) {
            arrGeneratorWords.push(strUniqueLetters);
          }
        }
      }

      file.contents = new Buffer(JSON.stringify({
        objWordsByPoints: objWordsByPoints,
        arrGeneratorWords: discardDupes(arrGeneratorWords)
      }));

      return file;
    }))
    .pipe(plugins.rename({extname: '.json'}))
    .pipe(gulp.dest(paths.dest.words));
});


/*
  Javascript:
  - Minify.
  - Concatenate into one file (scripts.js).
  - Write sourcemaps.
*/
gulp.task('js', function() {
  var filename = 'scripts.js';

  return gulp.src([
      paths.src.js + 'ApiaryLib/*.js',
      paths.src.js + 'ApiaryLib/classes/*.js',
      paths.src.js + '*.js'
    ])
    .pipe(handleErrors())
    .pipe(plugins.newer(paths.dest.js + filename))
    .pipe(plugins.sourcemaps.init())
    .pipe(plugins.if(args.production, plugins.uglify()))
    .pipe(plugins.concat(filename))
    .pipe(plugins.sourcemaps.write('.'))
    .pipe(gulp.dest(paths.dest.js));
});


/*
  Generate documentation for JS:
*/
gulp.task('js-doc', function() {

  if (!args.production) {
    return gulp.src([paths.src.js + '**/*.js'], {read: false})
      .pipe(handleErrors())
      .pipe(plugins.newer(paths.dest.jsdoc))
      .pipe(plugins.jsdoc3({
        opts: {
          destination: paths.dest.jsdoc,
          readme: 'README.md'
        },
        templates: {
          theme: 'cosmo'
        }
      }));
  } else {
    return true;
  }
});


/*
  Stylesheets:
  - Compile CSS from SCSS.
  - Add vendor prefixes.
  - Write sourcemaps.
  - Inject changes into browser.
*/
gulp.task('style', function() {
  return gulp.src(paths.src.sass + '**/*.scss')
    .pipe(handleErrors())
    .pipe(plugins.newer(paths.dest.css + '**/*.css'))
    .pipe(plugins.sourcemaps.init())
    .pipe(plugins.sass({
      outputStyle: args.production ? 'compressed' : 'expanded'
    }))
    .on('error', plugins.sass.logError)
    .pipe(plugins.autoprefixer({browsers: 'last 2 versions'}))
    .pipe(plugins.sourcemaps.write('.'))
    .pipe(gulp.dest(paths.dest.css))
    .pipe(bs.stream({match: '**/*.css'}));
});


/*
  Build only (don't serve or watch).
*/
gulp.task('build', [
  'vendor-js',
  'copy-assets',
  'build-wordlist',
  'jade',
  'js',
  'js-doc',
  'style'
]);



/*
  Default task (run with `gulp` from CLI).

  Build, then serve and watch for changes.
*/
gulp.task('default', ['build'], function() {

  // Start server
  bs.init({
    // see https://www.browsersync.io/docs/options
    server: paths.dest.root,
    notify: false,
    open: false
  });

  /*
    Watch for changes:

    In order to reload the browser only after the relevant tasks have completed, it's necessary to create an additional task that will use `bs.reload` as a callback, and then watch that task.
  */

  // vendor-js
  gulp.task('reload:vendor-js', ['vendor-js'], bs.reload);
  gulp.watch([paths.bower.root + '**/*'], ['reload:vendor-js']);

  // copy-assets
  gulp.task('reload:copy-assets', ['copy-assets'], bs.reload);
  gulp.watch([paths.src.assets + '**/*'], ['reload:copy-assets']);

  // build-wordlist
  gulp.task('reload:build-wordlist', ['build-wordlist'], bs.reload);
  gulp.watch([paths.src.words + '**/*'], ['reload:build-wordlist']);

  // jade
  gulp.task('reload:jade', ['jade'], bs.reload);
  gulp.watch([paths.src.jade + '**/*'], ['reload:jade']);

  // js, js-doc
  gulp.task('reload:js', ['js', 'js-doc'], bs.reload);
  gulp.watch([paths.src.js + '**/*'], ['reload:js']);

  // style (injects changes without reloading)
  gulp.watch([paths.src.sass + '**/*'], ['style']);

});
